<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Edit/Add</title>
</head>

<body>
    <div class="container mt-5">

        <?php echo form_open('user/store') ?>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" value="<?php echo isset($user['username']) ? $user['username'] : '' ?>" name="username" id="username" class="form-control">
            </div>

            <div class="form-group">
                <label for="first_name">Name</label>
                <input type="text" value="<?php echo isset($user['first_name']) ? $user['first_name'] : '' ?>" name="first_name" id="first_name" class="form-control">
            </div>

            <div class="form-group">
                <label for="last_name">Lastname</label>
                <input type="text" value="<?php echo isset($user['last_name']) ? $user['last_name'] : '' ?>" name="last_name" id="last_name" class="form-control">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" value="<?php echo isset($user['email']) ? $user['email'] : '' ?>" name="email" id="email" class="form-control">
            </div>

            <input type="submit" value="Salvar" class="btn btn-primary">
            <a href="javascript:window.history.go(-1);" class="btn btn-default">Back</a>
            <input type="hidden" name="id" value="<?php echo isset($user['id']) ? $user['id'] : '' ?>">
        <?php echo form_close(); ?>

    </div>
</body>

</html>