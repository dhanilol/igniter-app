<div class="container mt-5">
    <?php echo anchor(base_url('user/create'), 'New User', ['class' => 'btn btn-success mb-3']) ?>

    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Actions</th>
        </tr>
        <?php if ($users): ?>
            <?php foreach ($users as $user) : ?>
                <tr>
                    <td><?php echo $user['id'] ?></td>
                    <td><?php echo $user['first_name'] . " " . $user['last_name'] ?></td>
                    <td><?php echo $user['username'] ?></td>
                    <td><?php echo $user['email'] ?></td>
                    <td>
                        <?php echo anchor('user/edit/' . $user['id'], 'Edit', [
                            'class' => 'btn btn-success',
                        ]); ?>
                        
                        <?php echo anchor('user/delete/' . $user['id'], 'Remove', [
                            'class' => 'btn btn-danger',
                            'onClick' => 'return confirm()'
                        ]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
    <?php if ($users): ?>
        <?php echo $pager->links(); ?>
    <?php endif; ?>
</div>


<style>
    ul.pagination li {
        display: inline;
    }
    ul.pagination li a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
    }
    .active {
        background-color: #4CAF50;
        color: white;
    }
    ul.pagination li a:hover:not(.active) {
        background-color: #ddd;
    }
</style>

<script>
    function confirm() {
        if (!confirm('Are you sure you want to remove this user?')) {
            return false;
        }
        return true;
    }
</script>