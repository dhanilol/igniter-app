
<div class="container mt-5">
    <main class="form-signin col-md-6 offset-md-3">
        <form action="<?php echo base_url('auth/signIn') ?>" method="post">
            <div class="form-group">
                <!-- <label for="inputEmail" class="visually-hidden">Email address</label> -->
                <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            </div>

            <div class="form-group">
                <!-- <label for="inputPassword" class="visually-hidden">Password</label> -->
                <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
            </div>

            <button class="w-100 btn btn-lg btn-success form-control" type="submit">Sign in</button>
        </form>
        
        <div class="form-group mt-3">
            <a href="auth/register" class="w-100 btn btn-lg btn-primary form-control">Register</a>
        </div>

        <?php 
            $msg = session()->getFlashData('msg');
            $msgType = session()->getFlashData('msgType') ? session()->getFlashData('msgType') : 'danger';
        ?>

        <?php if (!empty($msg)) : ?>
            <div class="alert alert-<?= $msgType ?> mt-3">
                <?php echo $msg ?>
            </div>
        <?php endif; ?>
    </main>
</div>