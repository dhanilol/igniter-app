<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 90,
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'first_name' => [
				'type' => 'VARCHAR',
				'constraint' => 90,
			],
			'last_name' => [
				'type' => 'VARCHAR',
				'constraint' => 90,
			],
			'username' => [
				'type' => 'VARCHAR',
				'constraint' => 90,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('users');
	}

	public function down()
	{
		$this->forge->dropTable('users');
	}
}
