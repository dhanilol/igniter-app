<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;

class User extends BaseController
{
	private $userModel;

	public function __construct()
	{
		$this->userModel = new UserModel();
	}

	public function index()
	{
        $page = 'users';
        if ( ! is_file(APPPATH.'/Views/users/'. $page.'.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page); // Capitalize the first letter

        echo view('templates/header', $data);
		return view('Users/users', [
			'users' => $this->userModel->paginate(10),
			'pager' => $this->userModel->pager
		]);
	}

	public function delete($id)
	{
		if ($this->userModel->delete($id)) {
			echo view('messages', [
				'message' => 'User successfully removed!'
			]);
		} else {
			echo view('messages', [
				'message' => 'Error removing user'
			]);
		}
	}

	public function create()
	{
        if ( ! is_file(APPPATH.'/Views/users/user_form.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('user_form');
        }
        $data['title'] = ucfirst('New user'); // Capitalize the first letter

        echo view('templates/header', $data);
		return view('users/user_form');
	}

	public function store()
	{
		if ($this->userModel->save($this->request->getPost())) {
			return view("messages", [
				'message' => 'User successfully saved'
			]);
		} else {
			echo view('messages', [
				'message' => 'Error saving user'
			]);
		}
	}

	public function edit($id)
	{
        if ( ! is_file(APPPATH.'/Views/users/user_form.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('user_form');
        }
        $data['title'] = ucfirst('Edit user'); // Capitalize the first letter

        echo view('templates/header', $data);
		return view('users/user_form', [
			'user' => $this->userModel->find($id)
		]);
	}
}
