<?php

namespace App\Controllers;

use App\Models\UserModel;

class Auth extends BaseController
{
	public function index()
	{
        $page = 'login';
        if ( ! is_file(APPPATH.'/Views/auth/' . $page .'.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page); // Capitalize the first letter

        echo view('templates/header', $data);
		return view('auth/login');
	}

	public function register()
	{
		$page = 'register';
        if ( ! is_file(APPPATH.'/Views/auth/' . $page .'.php')) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page); // Capitalize the first letter

		if ($this->request->getPost()) {
			
			$data = $this->request->getPost();
			$data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

			if ((new UserModel())->save($data)) {
				session()->setFlashData('msg', 'Successfully Registered');
				session()->setFlashData('msgType', 'success');
				return redirect()->to('/auth');
			} else {
				session()->setFlashData('msg', 'Error registring user! Please try again.');
				return redirect()->to('/auth');
			}
		}

		echo view('templates/header', $data);
		return view('auth/register');
	}

	public function signIn()
	{
        $data['title'] = ucfirst('login');

        if (!$this->request->getPost()) {
            echo view('templates/header', $data);
		    return view('auth/login');
        }
        
		$email = $this->request->getPost('inputEmail');
		$password = $this->request->getPost('inputPassword');
        
		$userModel = new UserModel();
        
		$data = $userModel->getByEmail($email);
        
		if (count($data) > 0) {
			$userHash = $data['password'];

			if (password_verify($password, $userHash)) {
				session()->set('isLoggedIn', true);
				session()->set('name', $data['first_name']);
				return redirect()->to(base_url());
			} else {
				session()->setFlashData('msg', 'Invalid email or password! Try Again');
				return redirect()->to('/auth');
			}
		} else {
			session()->setFlashData('msg', 'Invalid email or password! Try Again');
			return redirect()->to('/auth');
		}
	}

	public function signOut()
	{
		session()->destroy();
		return redirect()->to(base_url());
	}
}
